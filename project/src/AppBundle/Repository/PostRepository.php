<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Post;
use Doctrine\ORM\EntityRepository;

/**
 * Class PostRepository
 *
 * @package AppBundle\Repository
 */
class PostRepository extends EntityRepository
{
    public function findPublished()
    {
        $post = $this->getEntityManager()->getRepository(Post::class)->createQueryBuilder('p')
            ->where('p.postAt < CURRENT_TIMESTAMP()')
            ->getQuery();
        $result = $post->getResult();
        return $result;
    }
}

