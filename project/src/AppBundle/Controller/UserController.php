<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * PostController constructor.
     *
     * @param UserService    $service
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserService $service,
        UserRepository $userRepository
    ) {
        $this->service = $service;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/admin/{page}/{pageId}/{_locale}", methods={"GET"},
     *     name="user_index", requirements={"page"="[a-zA-Z]+","pageId"="\d+"},
     *     defaults={"_locale"="en"})
     *
     * @return Response
     */
    public function indexAction()
    {
        $response = $this->userRepository->findAll();
        return $this->render('/templates/user/userinfo.html.twig',
            ['response' => $response]);
    }

    /**
     * @param $userId
     *
     * @return string
     * @Route("/admin/{userId}/{_locale}", methods={"GET"}, name="user_show",defaults={"_locale"="en"},
     *     requirements={"userId"="\d+"})
     *
     */
    public function showAction($userId)
    {
        $response = $this->userRepository->findOneById($userId);
        if ($response = null) {
            throw $this->createNotFoundException();
        }
        return $this->render('/templates/user/usershow.html.twig',
            ['response' => $response]);
    }

    /**
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $encoder
     * @param                              $userId
     *
     * @return string
     * @Route("/admin/{userId}/edit/{_locale}", methods={"GET","POST"}, name="user_edit",defaults={"_locale"="en"},
     *     requirements={"id"="\d+"})
     */
    public function editAction(Request $request, UserPasswordEncoderInterface $encoder, $userId = null)
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->findOneById($userId);
        if ($user = null) {
            throw $this->createNotFoundException();
        } else {
            $form = $this->createForm(UserType::class, $user);
            $password = $user->getPassword();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if ($user->getPassword() !== null){
                    $password = $encoder->encodePassword($user, $data->getPassword());
                }
                $data->setPassword($password);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($data);
                $entityManager->flush();
                return $this->redirectToRoute('post_list');
            } else {
                return $this->render('/form/usertype.html.twig', [
                    'form' => $form->createView(),
                    'post' => $user
                ]);
            }
        }
    }

    /**
     * @Route("/admin/new/{_locale}", methods={"GET","POST"}, name="user_new",defaults={"_locale"="en"})
     *
     * @param Request                      $request
     *
     * @param UserPasswordEncoderInterface $encoder
     *
     * @return Response
     */
    public function newAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $password = $encoder->encodePassword($user, $data->getPassword());
                $data->setPassword($password);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
                return $this->redirectToRoute('post_list');
            }
        }
        return $this->render('/form/usertype.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $id
     * @Route("/admin/{id}/delete/{_locale}", methods={"DELETE"}, name="user_delete",
     *     requirements={"id"="\d+"}, defaults={"_locale"="en"})
     *
     * @return string
     */
    public function deleteAction($id)
    {
        $response = $this->service->findOneById($id);
        if (!$response) {
            return new Response("Not found", 404);
        }
        $this->addFlash('notice', " This $id was deleted");
        return $this->redirectToRoute('user_index');
    }
}

