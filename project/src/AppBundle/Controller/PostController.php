<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use AppBundle\Repository\PostRepository;
use AppBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostController constructor.
     *
     *
     * @param UserService $service
     * @param PostRepository $postRepository
     */
    public function __construct(
        UserService $service,
        PostRepository $postRepository
    ) {
        $this->service = $service;
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("/admin/user/{page}/{pageId}/{_locale}", methods={"GET"},
     *     name="post_index", requirements={"page"="[a-zA-Z]+","pageId"="\d+"},
     *     defaults={"_locale"="en"})
     *
     * @return Response
     */
    public function indexAction()
    {
        $response = $this->postRepository->findAll();
        return $this->render('/templates/post/postinfo.html.twig',
            ['response' => $response]);
    }

    /**
     * @param $userId
     *
     * @return string
     * @Route("/admin/user/{userId}/{_locale}", methods={"GET"}, name="post_show",
     *     requirements={"userId"="\d+"}, defaults={"_locale"="en"})
     *
     */
    public function showAction($userId)
    {
        $response = $this->postRepository->findOneById($userId);
        if (!$response) {
            throw new NotFoundHttpException();
        }
        return $this->render('/templates/post/postshow.html.twig',
            ['response' => $response]);
    }

    /**
     * @param Request $request
     * @param         $postId
     *
     * @return string
     * @Route("/admin/user/{id}/edit/{_locale}", methods={"GET","POST"}, name="post_edit",
     *     requirements={"id"="\d+"}, defaults={"_locale"="en"})
     *
     */
    public function editAction(Request $request, $postId)
    {
        /**
         * @var Post $post
         */
        $post = $this->postRepository->findOneById($postId);
        if ($post = null) {
            throw $this->createNotFoundException();
        } else {
            $form = $this->createForm(PostType::class, $post);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($post);
                $entityManager->flush();
                return $this->redirectToRoute('post_list');
            } else {
                return $this->render('/form/posttype.html.twig', [
                    'form' => $form->createView(),
                ]);
            }
        }
    }

    /**
     * @Route("/admin/user/new/{_locale}", methods={"GET","POST"}, name="post_new", defaults={"_locale"="en"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $post = $form->getData();
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($post);
                $entityManager->flush();
                return $this->redirectToRoute('post_list');
            }
        }
        return $this->render('/form/posttype.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $id
     * @Route("/admin/user/{id}/delete/{_locale}", methods={"DELETE"}, name="post_delete",
     *     requirements={"id"="\d+"}, defaults={"_locale"="en"})
     *
     * @return string
     */
    public function deleteAction($id)
    {
        $response = $this->service->findOneById($id);
        if (!$response) {
            throw new NotFoundHttpException();
        }
        $this->addFlash('notice', "This $response was deleted");
        return $this->redirectToRoute('post_index');
    }

    /**
     * @return Response
     * @Route("/post/{pageId}/{_locale}", methods={"GET"}, name="post_list",
     *     requirements={"pageId"="\d+"}, defaults={"_locale"="en"})
     *
     */
    public function listAction()
    {
        $response = $this->postRepository->findPublished();
        return $this->render('/templates/post/postlist.html.twig',
            ['response' => $response]);
    }

    /**
     * @param $slug
     * @Route("/post/{slug}/{_locale}", methods={"GET"}, name="post_view",
     *     requirements={"slug"="[a-zA-Z]+"}, defaults={"_locale"="en"})
     *
     * @return string
     */
    public function viewAction($slug)
    {
        $response = $this->postRepository->findOneBy((array)'slug', array($slug));
        if (!$response) {
            throw new NotFoundHttpException();
        }
        return $this->render('/templates/post/postshow.html.twig',
            ['response' => $response]);
    }
}

