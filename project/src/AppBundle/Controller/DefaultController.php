<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/manager", name="product")
     */
   /* public function createAction()
    {
    $entityManager = $this->getDoctrine()->getManager();

    $product = new Product();
    $product->setName('KeyBoard');
    $product->setPrice(19.99);
    $product->setDescription('ergonomic and stylish');

    $entityManager->persist($product);

    $entityManager->flush();

   var_dump($product);
   return new Response('Saved new product with id '.$product->getId());
    }
*/
    /**
     * @param $productId
     * @Route("/product/{name}", name="showProduct")
     *
     * @return Response
     */
    public function showAction($name)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(
            array('name'=>'KeyBoard')
        );
        if (!$product) {
            throw $this->createNotFoundException('no product found for id '.$name);
        }
        var_dump($product);
        return new Response('Show product with name '. $name);
    }

    /**
     * @param $productId
     * @Route("/product1/{productId}")
     * @return RedirectResponse
     */
    public function updateAction($productId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($productId);

        if (!$product) {
            throw $this->createNotFoundException('no product found for id '.$productId);
        }

        $product->setName('Key');
        $product->setPrice(19);
        $product->setDescription('stylish');

        $entityManager->flush();
        var_dump($product);
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/pr")
     */
    public function listAction()
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->FindAllOrdersByName("Key");
        $products1 = $this->getDoctrine()->getRepository(Product::class)->FindAllOrdersByPrice("90");
        var_dump($products);
        var_dump($products1);
        exit();
    }

    /**
     * Route("/admin1")
     */
    public function adminDashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // or add an optional message - seen by developers
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
    }
}
