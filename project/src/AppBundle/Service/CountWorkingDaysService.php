<?php

namespace AppBundle\Service;

class CountWorkingDaysService
{
    const EIGHTH_MARCH = '';
    const FIRST_JANUARY = '';
    const CHRISTMAS = '';
    const LABOR_DAY = '';

    public $holidayDays
        = [
            self::EIGHTH_MARCH,
            self::FIRST_JANUARY,
            self::CHRISTMAS,
            self::LABOR_DAY
        ];

    public function calculateAllDays($firstDate, $lastDate)
    {
        $startDate = strtotime($firstDate);
        $endDate = strtotime($lastDate);
        $numberOfSeconds = 86400;

        return $diffOfDays = ($endDate - $startDate) / $numberOfSeconds;
    }

    public function checkHoliday($currentDay, $holidayDays)
    {
        $result = null;
        foreach ($currentDay as $num) {
            if (in_array($num, $holidayDays)) {
                $result[$num] = true;
            }
        }
        return $result;
    }

    public function checkWeekend($currentDay)
    {
        $checkedDay = date('w', $currentDay);

        return ($checkedDay == 0 || $checkedDay == 6);
    }

    public function calculateHolidayDays($firstDate, $diffOfDays, $holidayDays)
    {
        $startDate = strtotime($firstDate);
        $isHoliday = $this->checkHoliday($startDate, $holidayDays);
        $isWeekend = $this->checkWeekend($startDate);
        $countHolidayDays = 0;
        for ($i = 0; $i < $diffOfDays; $i++) {
            while ($isHoliday || $isWeekend) {
                $countHolidayDays++;
            }
        }
        return $countHolidayDays;
    }

    public function calculateWorkingDays($firstDate, $lastDate, $holidayDays)
    {
        $totalNumberOfDays = $this->calculateAllDays($firstDate, $lastDate);
        $countHolidayDays = $this->calculateHolidayDays($firstDate,
            $totalNumberOfDays, $holidayDays);

        return $totalNumberOfDays - $countHolidayDays;
    }
}

