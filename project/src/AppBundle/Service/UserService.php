<?php

namespace AppBundle\Service;

class UserService
{
    public function findAll()
    {
        $uniqueArray = [];

        return $uniqueArray;
    }

    /**
     * @param $id
     *
     * @return string|null
     */
    public function findOneById($id)
    {
        $uniqueArray = $this->findAll();
        if (!array_key_exists($id, $uniqueArray)) {
            return null;
        }
        return $uniqueArray[$id];
    }

    /**
     * @param $slug
     *
     * @return array
     */
    public function findOneBySlug($slug)
    {
        $uniqueArray = $this->findAll();
        $slugValues = [];
        array_unshift($slugValues, $slug);
        if (array_intersect($slugValues, $uniqueArray)) {
            return $slugValues;
        }
        return null;
    }
}

